# mycroft-youtube: Skill for playing youtube audio with mycroft
Based on the outdated https://github.com/augustnmonteiro/mycroft-youtube

...and https://gitlab.com/glor/mycroft-youtube.git which didn't quite work for me.

This is still in a very bad shape and should at one point be completely rewritten, but at least it works.
## for the user
### Requirements
You need vlc media player. You can install that on Debian based linux (e.g. Ubuntu) with:
```
sudo apt-get install vlc
```
### Install Using MSM (Mycroft Skill Manager)
This is probably the most convenient and safe way to install the skill. You should probably read this first: https://mycroft.ai/documentation/msm/
```
msm install https://gitlab.com/mcdruid/mycroft-youtube.git
```
You can find `msm` in `bin/mycroft-msm` in your mycroft instance directory.

### How it works
The skill searches on youtube and plays the audio of the first 10 results.

### How to use
- say `youtube guns 'n roses don't cry`
- say `stop youtube`
- say `youtube coldplay`

Full sentences are not supported.

## for everyone else
## Install Manually (Debian based linux e.g. Ubuntu)
```
sudo apt-get install vlc
sudo mkdir -p /opt/mycroft/skills
cd /opt/mycroft/skills 
sudo git clone https://gitlab.com/mcdruid/mycroft-youtube.git youtube
cd youtube
pip install -r requirements.txt 
```

## technical background
The skill uses the youtube search from python and parses the video urls from the result. It gets an audio stream urls using `pafy`. These are played as a playlist using `libVLC` and it's python binding `python-vlc`.

## TODO
- use [playback control skill](https://github.com/MycroftAI/skill-playback-control)
- add features: play one song, play endlessly, play similar songs
- more flexible vocabulary
