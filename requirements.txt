requests>=1.1
python-vlc>=3.0.6109
pafy>=0.5.4
youtube_dl>=2019.9.1
urllib3>=1.24.1
lxml>=4.3.4
