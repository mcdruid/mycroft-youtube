# -*- coding: utf-8 -*-

import time
import urllib
from os.path import dirname

try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

try:
    from urllib import quote 
except ImportError:
    from urllib.parse import quote
    
from adapt.intent import IntentBuilder
from bs4 import BeautifulSoup

import pafy
import urllib.request
from urllib.parse import *
import vlc

from mycroft.skills.core import MycroftSkill

__author__ = 'mcdruid'

class YoutubeSkill(MycroftSkill):
    def __init__(self):
        super(YoutubeSkill, self).__init__(name="YoutubeSkill")
        self.process = None

    def initialize(self):
        self.load_data_files(dirname(__file__))

        youtube = IntentBuilder("YoutubeKeyword"). \
            require("YoutubeKeyword").build()
        self.register_intent(youtube, self.youtube)
        self.register_intent(IntentBuilder("StopYoutubeKeyword").require("StopYoutubeKeyword").build(), self.stop_youtube)

    def url_search(self, search_string, amount=1):
        dict = []

        textToSearch = search_string
        query = urllib.parse.quote(textToSearch)
        url = "https://www.youtube.com/results?sp=EgIQAQ%253D%253D&search_query=" + quote(query)
        response = urllib.request.urlopen(url)
        html = response.read()
        soup = BeautifulSoup(html, 'lxml')
        for vid in soup.findAll(attrs={'class':'yt-uix-tile-link'}):
            if len(dict) < amount:
                dict.append('https://www.youtube.com' + vid['href'])
            else:
                break
        return dict

    Instance = vlc.Instance()
    list_player = Instance.media_list_player_new()

    def lucky_play(self, query, amount):
        MediaList = self.Instance.media_list_new()
        self.list_player.stop()
        self.list_player.set_media_list(MediaList)
        urls = self.url_search(query, amount)
        for i in range(len(urls)):
            # this takes ~1 second
            audio = pafy.new(urls[i])
            audiostream = audio.getbestaudio().url
            Media = self.Instance.media_new(audiostream)
            Media.get_mrl()
            MediaList.add_media(Media)
            # starts player if it isn't playing
            self.list_player.play()

    def youtube(self, message):
        self.stop()
        utterance = message.data.get('utterance').lower()
        utterance = utterance.replace(message.data.get('YoutubeKeyword'), '')
        self.speak("Playing songs for " + utterance);
        try:
            self.lucky_play(utterance, 10)
        except Exception as e:
            return self.speak("Error. Not all songs could be loaded correctly. " + e)
    
    def stop_youtube(self, message):
        self.list_player.stop()

    def stop(self):
        if self.process:
            self.process.terminate()
            self.process.wait()
        pass


def create_skill():
    return YoutubeSkill()
